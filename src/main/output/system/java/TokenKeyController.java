package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.TokenKey;
import com.zzwtec.community.model.TokenKeyM;
import com.zzwtec.community.model.TokenKeyMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.TokenKeyServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * TokenKey控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class TokenKeyController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY)
	public void list(){				
		//TokenKeyServicePrx prx = IceServiceUtil.getService(TokenKeyServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/token_key_list.html");
	}
	
	/**
	 * 添加TokenKey
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加TokenKey成功");;
		try{
			//TokenKeyServicePrx prx = IceServiceUtil.getService(TokenKeyServicePrx.class);
			TokenKey model = getBean(TokenKey.class);		
			//prx.addTokenKey(model);				
		}catch(Exception e){
			repJson = new DataObject("添加TokenKey失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除TokenKey
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除TokenKey成功");
		try{
			String ids = getPara("ids");		
			//TokenKeyServicePrx prx = IceServiceUtil.getService(TokenKeyServicePrx.class);
			//prx.delTokenKeyByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除TokenKey失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		TokenKey entity = new TokenKey();	
		setAttr("tokenKey", entity);
		render("/system/view/token_key_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//TokenKeyServicePrx prx = IceServiceUtil.getService(TokenKeyServicePrx.class);
			//TokenKeyM modele = prx.inspectTokenKey(id);
			TokenKey entity = new TokenKey();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("tokenKey", entity);			
			render("/system/view/token_key_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取TokenKey失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改TokenKey
	 */
	@ActionKey(UrlConstants.URL_TOKEN_KEY_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新TokenKey成功");
		try{
			//TokenKeyServicePrx prx = IceServiceUtil.getService(TokenKeyServicePrx.class);
			TokenKey model = getBean(TokenKey.class);
			//prx.alterTokenKey(model);
		}catch(Exception e){
			repJson = new DataObject("更新TokenKey失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(TokenKeyServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//TokenKeyMPage viewModel = prx.getTokenKeyList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		TokenKey[] aray = new TokenKey[12];		
		for(int i=0;i<12;i++){
			aray[i] = new TokenKey();
			aray[i].id = "id-"+i;			
		}	
		List<TokenKey> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
