package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.AddFormHtmlTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 添加表单构造器
 * @author yangtonggan
 * @date 2016-3-7
 */
public class AddFormHtmlBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		AddFormHtmlTemplate afht = new AddFormHtmlTemplate();
		afht.generate(vm, modularDir);
	}

}
