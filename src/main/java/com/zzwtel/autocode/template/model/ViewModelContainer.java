package com.zzwtel.autocode.template.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 模型容器
 * @author yangtonggan
 * @date 2016-3-7
 */
public class ViewModelContainer {
	
	private Map<String,TemplateModel> viewMap = new HashMap<String,TemplateModel>();
	
	public Map<String, TemplateModel> getViewMap() {
		return viewMap;
	}

	/**
	 * 添加视图到容器中
	 * @param key
	 * @param vm
	 */
	public void addView(String key,TemplateModel vm){
		viewMap.put(key, vm);
	}
	
	/**
	 * 从容器中删除视图
	 */
	public void removeView(String key){
		viewMap.remove(key);
	}
}
