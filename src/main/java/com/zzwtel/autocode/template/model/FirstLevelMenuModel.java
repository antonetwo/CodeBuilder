package com.zzwtel.autocode.template.model;


import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import us.codecraft.xsoup.Xsoup;

/**
 * 一级菜单模型
 * @author yangtonggan
 * @date 2016-6-8
 */
public class FirstLevelMenuModel extends MenuModel{
	//子菜单
	private List<SecondLevelMenuModel> childrenMenu = new ArrayList<SecondLevelMenuModel>();

	public List<SecondLevelMenuModel> getChildrenMenu() {
		return childrenMenu;
	}

	public void setChildrenMenu(List<SecondLevelMenuModel> childrenMenu) {
		this.childrenMenu = childrenMenu;
	}

	/**
	 * <first_level_menu name="一级菜单">
	   		<second_level_menu id="menu-xxxx-page" name="xxxx" />
	   </first_level_menu>
	 */
	@Override
	public String toXml() {
		StringBuilder sb = new StringBuilder();		
		sb.append(" <first_level_menu name=\""+getName()+"\">\n");  	
		for(SecondLevelMenuModel menu : childrenMenu){
			sb.append(menu.toXml());
		}
		sb.append(" </first_level_menu>\n");		
		return sb.toString();		
	}

	

	@Override
	public MenuModel fromElement(Element ele,int index) throws RuntimeException {		
		String name  = Xsoup.select(ele, "first_level_menu/@name").get();
		this.setIndex(index);
		this.setLevel(1);
		this.setName(name);
		Elements menus = ele.children();	
		int size = menus.size();	
		for(int i = 0 ; i<size ; i++){
			Element child = ele.child(i);
			SecondLevelMenuModel childMenu = new SecondLevelMenuModel();
			childMenu.fromElement(child, i);
			getChildrenMenu().add(childMenu);
		}		
		return this;
	}
	
}
